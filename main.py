import datetime
import time
from unicodedata import decimal

import matplotlib
from PyQt5.QtGui import QMovie
from PyQt5.uic.properties import QtCore
from PyQt5 import QtWidgets, uic, QtGui
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)
from matplotlib.backends.backend_qt4agg import (
    FigureCanvasQTAgg as FigureCanvas2)
import numpy as np
import MSSQLfce.getSpeed
import MSSQLfce.getProstoje as prostojFinder
import MSSQLfce.getnorm
import MSSQLfce.getprst as multistat
import matplotlib.dates as mdates
import MSSQLfce.getnormTAB as tabulkaNorem
import MSSQLfce.getIL as industrylog
import MSSQLfce.getDataProduction as ProductionData
import sys
from PyQt5.QtWidgets import QApplication, QTableWidgetItem
from PyQt5.QtCore import Qt, QDate, QTimer

app = QtWidgets.QApplication([])
window = QtWidgets.QMainWindow()
datewindow = QtWidgets.QMainWindow()
global SPID
SPID = "AAP50-01"
plt.style.use('Solarize_Light2')
fig1 = plt.figure()
fig2 = plt.figure()
fig3 = plt.figure()
fig4 = plt.figure()
gs = fig3.add_gridspec(2, 2)
ukaz1 = fig3.add_subplot(gs[0, 1])
tab1 = fig3.add_subplot(gs[0, 0])
prod = fig3.add_subplot(gs[1, 1])
# ax = fig2.add_axes([0, 0, 1, 1])
stat = fig3.add_subplot(gs[1, 0])
ax1f1 = fig1.add_subplot()
ax1f2 = fig1.add_subplot()
ax0210 = fig1.add_subplot()
ax0212 = fig1.add_subplot()
ax0219 = fig1.add_subplot()
ax1111 = fig1.add_subplot()
ax0000 = fig1.add_subplot()
ax0229 = fig1.add_subplot()
ax0309 = fig1.add_subplot()
ax0215 = fig1.add_subplot()
ax0228 = fig1.add_subplot()
ax0231 = fig1.add_subplot()
axnorma = fig1.add_subplot()
axdokoncenaetiketa = fig1.add_subplot()


def addmpl(fig):
    window.canvas = FigureCanvas(fig)
    # window.canvas.setParent(window.graf)
    window.grafLayout.addWidget(window.canvas)

    window.canvas.draw()


def addmpl2(fig):
    window.canvas2 = FigureCanvas(fig)
    # window.canvas.setParent(window.graf)
    window.grafLayout_2.addWidget(window.canvas2)

    window.canvas2.draw()


def datumyMiner(data):
    dates = []
    for y in data:
        dates.append(matplotlib.dates.date2num(datetime.datetime.fromtimestamp(y)))
    return dates


def hodnotyMiner(data):
    hodnota = []
    for x in data.values():
        hodnota.append(x)
    return hodnota


def setDate(casOD, casDO, noc, den2):
    global odCAS
    global doCAS
    if noc:
        datumOD = window.kalendarMain.selectedDate().addDays(-1).toString("yyyy-MM-dd")
    else:
        datumOD = window.kalendarMain.selectedDate().toString("yyyy-MM-dd")
    datumDO = window.kalendarMain.selectedDate().toString("yyyy-MM-dd")

    casdatumOD = datumOD + "T" + casOD
    casdatumDO = datumDO + "T" + casDO
    window.casodLB.setText(casdatumOD)
    window.casdoLB.setText(casdatumDO)
    odCAS = casdatumOD
    doCAS = casdatumDO
    loadMainGraf()


def loadMainGraf():
    global odCAS
    global doCAS
    datumyTEMP = 0
    valuesTEMP = 0
    speedTEMP = 0
    window.casodLB.setText(odCAS)
    window.casdoLB.setText(doCAS)
    movie = QMovie("anim/loader.gif")
    window.loadGIF.setMovie(movie)
    window.loadGIF.show()
    movie.start()
    window.label_2.setEnabled(False)
    window.kalendarMain.setMinimumDate(QDate.currentDate().addDays(-14))
    window.kalendarMain.setMaximumDate(QDate.currentDate())

    print(odCAS)
    print(doCAS)
    speedTEMP = MSSQLfce.getSpeed.dotaz(odCAS, doCAS, SPID)
    prostojALL = prostojFinder.dotaz(odCAS, doCAS, SPID, "0210")
    tab = tabulkaNorem.dotaz(odCAS, doCAS, SPID)
    ilTab = industrylog.dotaz(odCAS, doCAS, SPID)
    ilTabsub = industrylog.dotazGauge(odCAS, doCAS)




    production = ProductionData.dotaz(odCAS, doCAS, SPID)
    prod.cla()
    prod.set_xlim(0,100)
    prod.set_yticklabels([])
    tip=production[0]
    prod.barh("Produktivita", float(production[0]))
    prod.barh("Výkonnost", float(production[1]))
    for i, v in enumerate(production):
        if i==0:
            prod.text(0.5, i , "Produktivita")
        if i==1:
            prod.text(0.5, i , "Výkonnost")
        prod.text(float(production[i])-10, i , str(production[i]))




    ukaz1.cla()
    ukaz1.barh("OK", ilTabsub[0])
    ukaz1.barh("NOK", ilTabsub[1])
    ukaz1.barh("Rework", ilTabsub[2])
    ukaz1.set_title("Industry log")
    for i, v in enumerate(ilTabsub):
        ukaz1.text(v + 1, i, str(v))





    tab1.cla()
    for x in tab:
        tab1.barh(x[0], x[4])

    tab1.set_title("Tabulka Normy")
    tablist = list(tab)
    for i, v in enumerate(tablist):
        tab1.text(tablist[i][4], i, str(round(tablist[i][4], 2)))

    numrows = len(ilTab)
    if numrows > 0:  # 6 rows in your example
        numcols = len(ilTab[0])  # 3 columns in your example
    else:
        numcols = 0
    # Set colums and rows in QTableWidget
    window.ilTabulka.setColumnCount(numcols)
    window.ilTabulka.setRowCount(numrows)
    columns = ["Matnr", "Celkem", "OK", "NOK", "rework"]
    for row in range(numrows):
        for column in range(numcols):
            test = ilTab[row][column]
            window.ilTabulka.setItem(row, column, QTableWidgetItem((str(test))))
    window.ilTabulka.setHorizontalHeaderLabels(columns)
    window.ilTabulka.setColumnWidth(0, 150)
    window.ilTabulka.setColumnWidth(1, 100)
    addEnd = "2019-11-01T00:00:00"

    prostojALL[0].update({time.mktime(datetime.datetime.strptime(doCAS, "%Y-%m-%dT%H:%M:%S").timetuple()): 0})
    stat2 = multistat.dotaz(odCAS, doCAS, SPID)
    # stat2 = sorted(stat2.items(), key=lambda kv: (kv[1], kv[0]))
    datumyTEMP = datumyMiner(speedTEMP)
    valuesTEMP = hodnotyMiner(speedTEMP)
    if len(valuesTEMP) > 0:
        maxValue = max(valuesTEMP)
    else:
        maxValue = 1

    # fig1.patch.set_facecolor("#666262")
    # fig2.patch.set_facecolor("#666262")

    for x in prostojALL:
        for y, z in x.items():
            x[y] = z * maxValue

    norma = MSSQLfce.getnorm.dotaz(odCAS, doCAS, SPID)
    ax1f1.cla()
    ax1f2.cla()
    stat.cla()

    # ax1f1.set_facecolor("#403c3c")
    # ax1f1.xaxis.label.set_color("#d6d6d6")
    # ax1f1.tick_params(axis='both', colors="#d6d6d6")
    # ax1f1.set_xlabel('X-axis')
    ax1f1.fill_between(datumyTEMP, valuesTEMP, color="#6bc26b", alpha=0.2)
    ax1f1.plot_date(datumyTEMP, valuesTEMP, color="#6bc26b", ls="solid", label="Počet vyrobených kusů", linewidth=1.5)
    ax1f1.plot_date(datumyMiner(norma), hodnotyMiner(norma), drawstyle="steps-post", ls="solid", color="red",
                    marker=",", linewidth=5, label="Norma")

    ax1f1.fill_between(datumyMiner(prostojALL[0]), hodnotyMiner(prostojALL[0]), step="post",
                       alpha=0.4)
    ax1f1.plot_date(datumyMiner(prostojALL[0]), hodnotyMiner(prostojALL[0]), drawstyle="steps-post", ls="solid",
                    marker=".", color="C0", label="Prostoj 0210")

    ax1f1.fill_between(datumyMiner(prostojALL[1]), hodnotyMiner(prostojALL[1]), step="post",
                       alpha=0.4)
    ax1f1.plot_date(datumyMiner(prostojALL[1]), hodnotyMiner(prostojALL[1]), drawstyle="steps-post",
                    ls="solid", linewidth=1,
                    marker=".", color="C1", label="Prostoj 0212")

    ax1f1.fill_between(datumyMiner(prostojALL[2]), hodnotyMiner(prostojALL[2]), step="post",
                       alpha=0.4)
    ax1f1.plot_date(datumyMiner(prostojALL[2]), hodnotyMiner(prostojALL[2]), drawstyle="steps-post",
                    ls="solid", linewidth=1, color="C2",
                    marker=".", label="Prostoj 0219")

    ax1f1.fill_between(datumyMiner(prostojALL[3]), hodnotyMiner(prostojALL[3]), step="post",
                       alpha=0.4)
    ax1f1.plot_date(datumyMiner(prostojALL[3]), hodnotyMiner(prostojALL[3]), drawstyle="steps-post",
                    ls="solid", linewidth=1, color="C3",
                    marker=".", label="Prostoj 1111")

    ax1f1.fill_between(datumyMiner(prostojALL[4]), hodnotyMiner(prostojALL[4]), step="post",
                       alpha=0.4)
    ax1f1.plot_date(datumyMiner(prostojALL[4]), hodnotyMiner(prostojALL[4]), drawstyle="steps-post",
                    ls="solid", linewidth=1, color="C4",
                    marker=".", label="Prostoj 0000")

    ax1f1.fill_between(datumyMiner(prostojALL[5]), hodnotyMiner(prostojALL[5]), step="post",
                       alpha=0.4)
    ax1f1.plot_date(datumyMiner(prostojALL[5]), hodnotyMiner(prostojALL[5]), drawstyle="steps-post",
                    ls="solid", linewidth=1, color="C5",
                    marker=".", label="Prostoj 0229")

    ax1f1.fill_between(datumyMiner(prostojALL[6]), hodnotyMiner(prostojALL[6]), step="post",
                       alpha=0.4)
    ax1f1.plot_date(datumyMiner(prostojALL[6]), hodnotyMiner(prostojALL[6]), drawstyle="steps-post",
                    ls="solid", linewidth=1, color="C6",
                    marker=".", label="Prostoj 0309")

    ax1f1.fill_between(datumyMiner(prostojALL[7]), hodnotyMiner(prostojALL[7]), step="post",
                       alpha=0.4)
    ax1f1.plot_date(datumyMiner(prostojALL[7]), hodnotyMiner(prostojALL[7]), drawstyle="steps-post",
                    ls="solid", linewidth=1, color="C7",
                    marker=".", label="Prostoj 0215")

    ax1f1.fill_between(datumyMiner(prostojALL[8]), hodnotyMiner(prostojALL[8]), step="post",
                       alpha=0.4)
    ax1f1.plot_date(datumyMiner(prostojALL[8]), hodnotyMiner(prostojALL[8]), drawstyle="steps-post",
                    ls="solid", linewidth=1, color="C8",
                    marker=".", label="Prostoj 0228")

    ax1f1.fill_between(datumyMiner(prostojALL[8]), hodnotyMiner(prostojALL[8]), step="post",
                       alpha=0.4)
    ax1f1.plot_date(datumyMiner(prostojALL[9]), hodnotyMiner(prostojALL[9]), drawstyle="steps-post",
                    ls="solid", linewidth=1, color="C9",
                    marker=".", label="Prostoj 0231")

    ax1f1.yaxis.set_label_position("right")
    ax1f1.yaxis.tick_right()
    ax1f1.legend(bbox_to_anchor=(-0.01, 1.05))

    box = stat.get_position()
    # stat.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # stat.set_facecolor("#403c3c")
    list_keys = [k for k in stat2]
    list_values = [v for v in stat2.values()]
    ukaz1.barh(1, 2)
    listKodu = []
    for index in range(len(list_keys)):
        stat.barh(list_keys[index], list_values[index], align='center', label=list_keys[index])
        listKodu.append(list_keys[index][:4])
    stat.set_yticklabels([])
    stat.invert_yaxis()  # labels read top-to-bottom
    stat.set_xlabel('Počet minut')
    stat.set_title('Prostoje')
    # stat.legend(bbox_to_anchor=(0.7, 1.05))
    # stat.grid(axis='x', color='k')
    # stat.tick_params(axis='both', colors="#d6d6d6")
    # addmpl2(fig2)

    for i, v in enumerate(list_keys):
        stat.text(0.5, i + .15, str(v))
        stat.text(list_values[i], i + .50, str(list_values[i]))
    ax1f1.grid(color='k')
    plt.draw()
    window.canvas.draw()
    window.canvas2.draw()
    movie.stop()
    window.loadGIF.hide()
    window.label_2.setEnabled(True)


def ranni():
    casod = "06:00:00"
    casdo = "14:00:00"
    setDate(casod, casdo, 0, 0)


def odpo():
    casod = "14:00:00"
    casdo = "22:00:00"
    setDate(casod, casdo, 0, 0)


def nocni():
    casod = "22:00:00"
    casdo = "06:00:00"
    setDate(casod, casdo, 1, 0)


def celyDen():
    casod = "00:00:00"
    casdo = "23:59:00"
    setDate(casod, casdo, 0, 0)


def dvaDny():
    global odCAS
    global doCAS
    odCAS = "2019-11-13T14:00:00"
    doCAS = "2019-11-13T22:00:00"


def main():
    with open('mainchart.ui', encoding='utf-8') as f:
        uic.loadUi(f, window)
    global odCAS
    global doCAS
    odCAS = "2019-11-13T14:00:00"
    doCAS = "2019-11-13T22:00:00"
    addmpl(fig1)
    # addmpl2(fig2)
    addmpl2(fig3)
    # addmpl2(fig4)
    loadMainGraf()
    window.ranBT.clicked.connect(ranni)
    window.odpoBT.clicked.connect(odpo)
    window.nocBT.clicked.connect(nocni)
    window.denBT.clicked.connect(celyDen)
    window.den2BT.clicked.connect(dvaDny)
    window.kalendarMain.setMinimumDate(QDate.currentDate().addDays(-14))
    window.kalendarMain.setMaximumDate(QDate.currentDate())
    window.showFullScreen()
    timer = QTimer()
    timer.timeout.connect(loadMainGraf)
    timer.start(30000)
    return app.exec()


main()

