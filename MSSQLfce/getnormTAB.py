import pymssql


def dotaz(od, do, SPID):
    result = []

    conn = pymssql.connect("10.100.2.6:49190", "janca", "skodovka", "Online_booking")

    cursor = conn.cursor()
    query = "SELECT DISTINCT v_utc_worklog_online.matnr,v_utc_worklog_online.workVariant, normMinutes " \
            "as NormoMinuty ,normQuantity,(normQuantity*1.0)/((normMinutes*1.0)/5.0) AS norma_5min_1_operator  " \
            "FROM [Online_booking].[dbo].[workNorm]  INNER JOIN  Online_booking.dbo.v_utc_worklog_online " \
            "ON Online_booking.dbo.v_utc_worklog_online.matnr=Online_booking.dbo.workNorm.matnr " \
            "AND Online_booking.dbo.v_utc_worklog_online.workVariant=Online_booking.dbo.workNorm.variantID " \
            "AND v_utc_worklog_online.ScanPointID='" + SPID + "'  WHERE v_utc_worklog_online.logFlag='NEW' " \
                                                              "and logtime BETWEEN '" + od + "' AND '" + do + "' " \
                                                                                                              "ORDER BY v_utc_worklog_online.matnr DESC"
    cursor.execute(query)
    row = cursor.fetchone()

    while row:
        if (row):
            result.append(row)

        row = cursor.fetchone()

    conn.close()

    return result
