import pymssql

global pros
pros = []


def vratProstoje():
    return pros


def dotazTAB(od, do, SPID):
    result = dict()

    conn = pymssql.connect("10.100.2.6:49190", "janca", "skodovka", "Online_booking")

    cursor = conn.cursor()
    query = """SELECT  
            DISTINCT v_utc_worklog_online.matnr,v_utc_worklog_online.workVariant,
            normMinutes as NormoMinuty
            ,normQuantity,
                  (normQuantity*1.0)/((normMinutes*1.0)/5.0) AS norma_5min_1_operator
              FROM [Online_booking].[dbo].[workNorm]
              INNER JOIN  Online_booking.dbo.v_utc_worklog_online ON 
              Online_booking.dbo.v_utc_worklog_online.matnr=Online_booking.dbo.workNorm.matnr 
              AND Online_booking.dbo.v_utc_worklog_online.workVariant=Online_booking.dbo.workNorm.variantID 
              AND v_utc_worklog_online.ScanPointID='"""+SPID+"""'
              WHERE v_utc_worklog_online.logFlag='NEW' and logtime BETWEEN '"""+od+"""' AND '"""+do+"""'
            ORDER BY v_utc_worklog_online.matnr DESC"""
    cursor.execute(query)
    row = cursor.fetchone()

    while row:
        if (row):
            result.update(list(row))

        row = cursor.fetchone()

    conn.close()

    return result

def dotaz(od, do, SPID):
    result = dict()

    conn = pymssql.connect("10.100.2.6:49190", "janca", "skodovka", "Online_booking")

    cursor = conn.cursor()
    query = "DECLARE @normaRAW int SELECT cas as time, pocetprac*normaRAW as norma_na_linku " \
            "FROM(SELECT dateadd(second,FLOOR(DATEDIFF(second, '1970-01-01', logtime)/300)*300, '1970-01-01') as cas, " \
            "Online_booking.dbo.v_worklog_online.staffString, " \
            "pocetprac=LEN(Online_booking.dbo.v_worklog_online.staffString) - " \
            "LEN(REPLACE(Online_booking.dbo.v_worklog_online.staffString, ',', ''))+1 ," \
            " normaRAW=((normQuantity*1.0)/((normMinutes*1.0)/5.0))" \
            " FROM [Online_booking].[dbo].[workNorm]" \
            " INNER JOIN Online_booking.dbo.v_worklog_online ON " \
            "Online_booking.dbo.v_worklog_online.matnr=Online_booking.dbo.workNorm.matnr " \
            "AND Online_booking.dbo.v_worklog_online.workVariant=Online_booking.dbo.workNorm.variantID " \
            "AND v_worklog_online.ScanPointID='" + str(SPID) + "' WHERE logtime BETWEEN '" + od + "' AND '" + do + "'" \
                                                                                                                   "and v_worklog_online.logFlag='NEW') as foo order by cas"
    cursor.execute(query)
    row = cursor.fetchone()

    while row:
        if (row):
            result.update({(row[0].timestamp()): row[1]})

        row = cursor.fetchone()

    conn.close()

    return result
